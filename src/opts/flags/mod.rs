use super::{Kind, Result, Unimplemented};

pub mod cflag;
pub mod iflag;
pub mod oflag;
#[cfg(test)]
mod tests;
