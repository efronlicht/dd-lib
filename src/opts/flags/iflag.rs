use super::{Kind, Result, Unimplemented};
use opts;
bitflags! {
    /// Input (read) options
    /// - append:  append mode (makes sense only for output; conv=notrunc
    /// suggested)
    ///
    /// - direct:  use direct I/O for data
    ///
    /// - directory:  fail unless a directory
    ///
    /// - dsync:  use synchronized I/O for data
    ///
    /// - sync:  use synchronized io for data AND metadata
    ///
    /// - fullblock:  accumulate full blocks of input
    ///
    /// - nonblock:  use non-blocking I/O
    ///
    /// - noatime:  do not update access time
    ///
    /// - nocache:  Request to drop cache.  See also oflag=sync
    ///
    /// - noctty:  do not assign controlling terminal from file
    ///
    /// - nofollow:  do not follow symlinks
    ///
    /// - count_bytes:  treat 'count=N' as a byte count
    ///
    /// - skip_bytes:  treat 'skip=N' as a byte count
    pub struct IFlag: u32 {
        /// append mode (makes sense only for output; conv=notrunc suggested)
        const APPEND = 0x0001;
        /// <NOT IMPLEMENTED> use direct I/O for data
        const DIRECT = 0x0002;
        /// fail unless a directory
        const DIRECTORY = 0x0004;
        /// <NOT IMPLEMENTED> use synchronized I/O for data
        const DSYNC = 0x0008;
        /// <UNIMPLMEMENTED> use synchronized I/O for data and metadata
        const SYNC =0x0010;
        /// accumulate full blocks of input (iflag only)
        const FULLBLOCK= 0x0020;
        /// <NOT IMPLEMENTED> use non-blocking I/O
        const NONBLOCK = 0x0040;
        /// <NOT IMPLEMENTED> do not update access time
        const NOATIME = 0x0080;
        /// <NOT IMPLEMENTED> Request to drop cache.  See also oflag=sync
        const NOCACHE = 0x0100;
        /// <NOT IMPLEMENTED> do not assign controlling terminal from file
        const NOCTTY = 0x0200;
        /// <NOT IMPLEMENTED> do not follow symlinks
        const NOFOLLOW = 0x0400;
        /// treat 'count=N' as a byte count (iflag only)
        const COUNT_BYTES = 0x0800;
        /// treat 'skip=N' as a byte count (iflag only)
        const SKIP_BYTES = 0x1000;
    }
}

impl IFlag {
    /// create a new `opts::IFlag` by parsing a comma-and-optional-whitespace
    /// separated list of arguments
    /// ```
    /// # use dd_lib::opts::IFlag;
    /// let want = IFlag::SYNC | IFlag::COUNT_BYTES;
    /// assert_eq!(want, IFlag::new("count_bytes,  sync  ").unwrap());
    /// ```
    pub fn new<S: AsRef<str>>(s: S) -> Result<Self> { s.as_ref().parse() }

    fn known_flag(flag: &str) -> Result<Self> {
        Ok(match flag {
            "append" => Self::APPEND,
            "directory" => Self::DIRECTORY,
            "dsync" => Self::DSYNC,
            "sync" => Self::SYNC,
            "fullblock" => Self::FULLBLOCK,
            "nonblock" => Self::NONBLOCK,
            "noatime" => Self::NOATIME,
            "nocache" => Self::NOCACHE,
            "noctty" => Self::NOCTTY,
            "nofollow" => Self::NOFOLLOW,
            "count_bytes" => Self::COUNT_BYTES,
            "skip_bytes" => Self::SKIP_BYTES,
            _ => return Self::unknown(flag.to_owned()),
        })
    }
}

impl std::str::FromStr for IFlag {
    type Err = opts::Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut flags = Self::default();
        for flag in s.split(',').map(str::trim).filter(|s| s.len() > 0) {
            Self::check_if_implemented(flag)?;
            flags |= { Self::known_flag(flag)? };
        }
        Ok(flags)
    }
}

impl Default for IFlag {
    fn default() -> IFlag { IFlag::empty() }
}

impl Unimplemented for IFlag {
    const KIND: Kind = Kind::IFlag;
    const UNIMPLEMENTED: &'static [&'static str] = &[
        "direct",
        "directory",
        "dync",
        "noatime",
        "nocache",
        "nofollow",
        "noctty",
        "nonblock",
        "fullblock",
    ];
}
