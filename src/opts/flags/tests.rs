use super::{cflag::CFlag, iflag::IFlag, oflag::OFlag, Unimplemented};
#[test]
fn oflag() {
    assert!("   \n \n ".parse::<OFlag>().unwrap().is_empty());

    assert!(OFlag::UNIMPLEMENTED[0].parse::<OFlag>().is_err());
    let args = "\n\n\n        sync       ";
    let oflag: OFlag = args.parse().unwrap();
    assert_eq!(oflag, OFlag::SYNC);
}

#[test]
fn iflag() {
    use std::str::FromStr;
    assert!("".parse::<IFlag>().unwrap().is_empty());
    assert_eq!("append,    ".parse::<IFlag>().unwrap(), IFlag::APPEND);

    assert!(IFlag::from_str(IFlag::UNIMPLEMENTED[0]).is_err());

    let args = concat!("append,\n\t\t\t  ", "sync,\n", "count_bytes,\n", "skip_bytes,\n",);
    assert_eq!(
        IFlag::from_str(args).unwrap(),
        IFlag::APPEND | IFlag::SYNC | IFlag::COUNT_BYTES | IFlag::SKIP_BYTES
    );
}
#[test]
fn cflag() {
    assert!("\t\t\t\t\t   \n\r".parse::<CFlag>().unwrap().is_empty());
    assert_eq!(
        CFlag::LCASE | CFlag::NOTRUNC,
        "lcase  , \t\t notrunc ".parse::<CFlag>().unwrap()
    );
}
