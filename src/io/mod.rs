//! Reading, writing, and manipulating bytes.
//!
//! In this module, we use the following type shorthands:
//! - R is the [`Reader`]: for dd, this is either [`Stdin`][std::io::StdinLock]
//!   or a
//! [`File`][std::fs::File]
//! - W is the [`Writer`]: for dd, this is either   [std::io::StdoutLock]
//! or a [`File`][std::fs::File]
//! - C is a [`ConvertSlice`], which modifies a slice of bytes in-place. During
//!   operation, this is always a [`Converter`]
//! - E is the [`ErrHandler`]. During operation, this is  [std::io::StderrLock].

/// copy bytes from a [`Reader`] to a [`Writer`]
/// converting them according to a [`Converter`]
pub mod copy;

mod conv;
mod errhandler;
mod read;
mod write;

#[cfg(test)]
mod tests;

/// [`Converter`] encodes a slice to the specified encoding,
/// changes the case (if applicable), and/or swaps pairs of bytes,
/// according to the conversion flags and conversion block size specified in
/// [`opts`]
pub use self::conv::Converter;

/// [`ErrHandler`] handles errors, writing errors and/or perodic reports to the
/// speicifed file or stderr. see [`StatusLevel`] and
/// [`NOERROR`][CFlag]
pub use self::errhandler::ErrHandler;

/// [`Reader`] reads from the specified file or stdin, according
/// to the options specified in `opts`
pub use self::read::Reader;

/// [`Writer`] writes to the specified file or stdout, according
/// to the options specified in `opts`
pub use self::write::Writer;

pub use self::conv::{EncodeTo, Case, ConvertSlice};

#[allow(unused_imports)]
use opts::{self, CFlag, StatusLevel};
